//
//  ViewController.swift
//  googleSignInwithFirebase
//
//  Created by as on 1/25/20.
//  Copyright © 2020 as. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class ViewController: UIViewController {
    
    
    @IBOutlet weak var loginBtn: GIDSignInButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
  GIDSignIn.sharedInstance()?.presentingViewController = self
//
    }

    @IBAction func loginBtn(_ sender: UIButton) {
        
       // GIDSignIn.sharedInstance()?.signIn()

        
        GIDSignIn.sharedInstance()?.signIn()
        
        
    }
    
}

